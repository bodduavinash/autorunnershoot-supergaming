using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sprite Shapes", menuName = "ScriptableObjects/SpriteShapesScriptableObject", order = 1)]
public class SpriteShapesSO : ScriptableObject
{
    public List<SpriteShapeData> spriteList;
}
