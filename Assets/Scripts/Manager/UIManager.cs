using UnityEngine;

public class UIManager : SingletonWithMonoBehavior<UIManager>
{
    private GameObject ScreensUI;
    private GameOverUIScreen gameOverScreen;
    private LaneUIScreen laneUIScreen;

    public readonly string ScreensTag = "Screens";

    private void Start()
    {
        ScreensUI = GameObject.FindWithTag(ScreensTag);
        gameOverScreen = ScreensUI.GetComponent<GameOverUIScreen>();
        laneUIScreen = ScreensUI.GetComponent<LaneUIScreen>();

        ShowLaneUI(true);
    }

    public void ShowGameoverUI(bool show)
    {
        gameOverScreen.ShowGameOverScreen(show);
    }

    public void ShowLaneUI(bool show)
    {
        laneUIScreen.ShowLaneUIScreen(show);
    }
}