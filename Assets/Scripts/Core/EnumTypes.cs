
public enum ShapeTypeEnum
{
    Square,
    Circle,
    Triangle
}

public enum LaneEnum
{
    Lane1,
    Lane2,
    Lane3
}

public enum ObjectTypeEnum
{
    Enemy,
    Obstacle
}