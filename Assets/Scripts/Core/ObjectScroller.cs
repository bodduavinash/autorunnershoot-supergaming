using UnityEngine;

public class ObjectScroller : MonoBehaviour
{
    [Range(1f, 3f)]
    private float speed = 0f;

    private void Start()
    {   
        speed = Random.Range(1f, 3f);
    }

    private void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * speed);
    }
}
