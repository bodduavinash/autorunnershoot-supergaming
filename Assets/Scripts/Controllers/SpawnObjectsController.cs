using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjectsController : MonoBehaviour
{
    public GameObject obstacleEmptyPrefab;
    public GameObject enemyEmptyPrefab;

    public GameObject Lane1GameObject;
    public GameObject Lane2GameObject;
    public GameObject Lane3GameObject;

    public SpriteShapesSO shapesSO;

    private GameObjectPool ObstacleShapePool;
    private GameObjectPool EnemyShapePool;

    private List<GameObject> activeEnemyObjectList;

    public List<GameObject> ActiveEnemyObjectList { get => activeEnemyObjectList;}

    private void Start()
    {
        ObstacleShapePool = new GameObjectPool("ObstacleShapePool");
        EnemyShapePool = new GameObjectPool("EnemyShapePool");

        activeEnemyObjectList = new List<GameObject>();

        ObstacleShapePool.InitGameObjectsPool(obstacleEmptyPrefab, 5);
        EnemyShapePool.InitGameObjectsPool(enemyEmptyPrefab, 5);
    }

    public GameObject SpawnObstacleObject()
    {
        return ObstacleShapePool.GetObjectFromPool();
    }

    public GameObject SpawnEnemyObject()
    {
        return EnemyShapePool.GetObjectFromPool();
    }

    public void ReturnObstacleObjectToPool(GameObject gameObject)
    {
        ObstacleShapePool.ReturnObjectToPool(gameObject);
    }

    public void ReturnEnemyObjectToPool(GameObject gameObject)
    {
        EnemyShapePool.ReturnObjectToPool(gameObject);
        activeEnemyObjectList.Remove(activeEnemyObjectList.Find(x => x == gameObject));
    }

    public void InitSpawnObjects(LaneEnum playerLane)
    {
        for (var i = 0; i < 3; i++)
        {
            if ((int)playerLane == i)
            {
                SpawnObjects(playerLane, ObjectTypeEnum.Obstacle);
            }
            else
            {
                SpawnObjects((LaneEnum)i, ObjectTypeEnum.Enemy);
            }
        }
    }

    public SpriteShapeData SpawnObjects(LaneEnum lane, ObjectTypeEnum objectType)
    {
        //Spawn object at lane
        var parentsLaneGameObject = GetLaneGameObject(lane);
        var gameObject = GetSpawnObjectOfType(objectType);
        var shapeType = GetRandomSpriteShape();

        gameObject.transform.position = parentsLaneGameObject.transform.position;
        gameObject.GetComponent<SpriteRenderer>().sprite = shapeType.sprite;

        //Assign gameobject to parent's lane
        gameObject.transform.parent = parentsLaneGameObject.transform;

        if (objectType.Equals(ObjectTypeEnum.Enemy))
        {
            activeEnemyObjectList.Add(gameObject);
        }

        return shapeType;
    }

    private GameObject GetSpawnObjectOfType(ObjectTypeEnum objectType)
    {
        return objectType == ObjectTypeEnum.Obstacle ? SpawnObstacleObject() : SpawnEnemyObject();
    }

    private GameObject GetLaneGameObject(LaneEnum lane)
    {
        switch(lane)
        {
            case LaneEnum.Lane1:
                return Lane1GameObject;

            case LaneEnum.Lane2:
                return Lane2GameObject;

            case LaneEnum.Lane3:
                return Lane3GameObject;
        }

        return null;
    }

    public GameObject GetEnemySpawnObjectOfShapeType(ShapeTypeEnum shapeType)
    {
        foreach(var enemyObject in activeEnemyObjectList)
        {
            var sprite = enemyObject.GetComponent<SpriteRenderer>().sprite;
            if (sprite.name.Equals(shapeType.ToString()))
            {
                return enemyObject;
            }
        }

        return null;
    }

    private SpriteShapeData GetRandomSpriteShape()
    {
        var randShapeType = UnityEngine.Random.Range(0, Enum.GetValues(typeof(ShapeTypeEnum)).Length);
        return shapesSO.spriteList.Find(x => x.shapesType.Equals((ShapeTypeEnum)randShapeType));
    }
}