using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeController : MonoBehaviour, IChangeShape
{
    public SpriteShapesSO shapesSO;

    private SpriteRenderer spriteRenderer;
    private ShapeTypeEnum currentShapeType;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void ChangeShape(ShapeTypeEnum _shapesType)
    {
        if (spriteRenderer == null)
            Debug.LogError("Sprite Renderer instance is not found");

        currentShapeType = _shapesType;

        spriteRenderer.sprite = GetSpriteOfShapeType(_shapesType);
    }

    public Sprite GetSpriteOfShapeType(ShapeTypeEnum _shapesType)
    {
        return shapesSO.spriteList.Find(x => x.shapesType.Equals(_shapesType)).sprite;
    }

    public SpriteShapeData GetSpriteData()
    {
        return shapesSO.spriteList.Find(x => x.shapesType.Equals(currentShapeType));
    }
}