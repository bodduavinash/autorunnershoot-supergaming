using UnityEngine;

public class PlayerController : SingletonWithMonoBehavior<PlayerController>
{
    private readonly string Player1Tag = "Player1";
    private readonly string Player2Tag = "Player2";
    private readonly string ObstacleTag = "Obstacle";
    public readonly string EnemyTag = "Enemy";
    public readonly string BulletTag = "Bullet";

    public SpriteShapesSO shapesSO;
    public GameObject bulletPrefab;

    public SpawnObjectsController SpawnController { get => spawnController;}
    public Player1Controller Player1Controller { get => player1Controller;}
    public Player2Controller Player2Controller { get => player2Controller;}

    private ShapeController Player1;
    private ShapeController Player2;

    private Player1Controller player1Controller;
    private Player2Controller player2Controller;

    private IUpdatePlayerInput playerInput;
    private SpawnObjectsController spawnController;

    private float playerLaneDistance = 3f;
    private Vector3 previousLanePosition;

    private void Start()
    {
        Player1 = GameObject.FindWithTag(Player1Tag).GetComponent<ShapeController>();
        Player2 = GameObject.FindWithTag(Player2Tag).GetComponent<ShapeController>();

        spawnController = Transform.FindObjectOfType<SpawnObjectsController>();

        playerInput = new PlayerInput(Player1, Player2);

        player1Controller = new Player1Controller(Player1, bulletPrefab, spawnController);
        player2Controller = new Player2Controller(Player2, spawnController);

        previousLanePosition = transform.position;
    }

    private void Update()
    {
        playerInput.OnUpdatePlayerInput();
        player1Controller.Update();
    }

    public void SetPlayerLane(LaneEnum laneType)
    {
        switch(laneType)
        {
            case LaneEnum.Lane1:
                transform.position = Vector3.left * playerLaneDistance + previousLanePosition;
                break;

            case LaneEnum.Lane2:
                transform.position = Vector3.zero + previousLanePosition;
                break;

            case LaneEnum.Lane3:
                transform.position = Vector3.right * playerLaneDistance + previousLanePosition;
                break;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals(ObstacleTag))
        {
            player2Controller.OnTriggerEnter2D(collision, shapesSO);
        }
    }
}