using System;
using UnityEngine;

public class EnemyDamageController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag(PlayerController.Instance.BulletTag))
        {
            if (transform.parent != null)
            {
                Enum.TryParse<LaneEnum>(transform.parent.name.ToString(), out var lane);

                PlayerController.Instance.SpawnController.ReturnEnemyObjectToPool(gameObject);
                PlayerController.Instance.Player1Controller.BulletProjectileController.ReturnBulletToObjectPool(collision.gameObject);

                PlayerController.Instance.SpawnController.SpawnObjects(lane, ObjectTypeEnum.Enemy);
            }
        }
        if(collision.CompareTag(PlayerController.Instance.EnemyTag))
        {
            UIManager.Instance.ShowGameoverUI(true);
        }
    }
}