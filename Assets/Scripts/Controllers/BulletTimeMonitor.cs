using UnityEngine;

public class BulletTimeMonitor : MonoBehaviour
{
    private const float bulletLifeTime = 2f;
    private float bulletTime;

    private void OnEnable()
    {
        bulletTime = bulletLifeTime;
    }

    private void Update()
    {
        if (bulletTime <= 0f)
        {
            PlayerController.Instance.Player1Controller.BulletProjectileController.ReturnBulletToObjectPool(this.gameObject);
        }

        bulletTime -= Time.deltaTime;
    }
}
