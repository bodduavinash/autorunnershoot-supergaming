using UnityEngine;

public class BulletProjectileController
{
    private ShapeController Player1;
    private SpawnObjectsController spawnController;
    private GameObjectPool bulletObjectPool;

    private const float nextFireRate = 2f;
    private float currentTime = 0f;

    private float bulletSpeed = 100f;

    public BulletProjectileController(ShapeController Player, string poolName, GameObject bulletPrefab, SpawnObjectsController spawnObjectsController)
    {
        Player1 = Player;
        bulletObjectPool = new GameObjectPool(poolName);
        bulletObjectPool.InitGameObjectsPool(bulletPrefab, 10);
        spawnController = spawnObjectsController;
    }

    public void ReturnBulletToObjectPool(GameObject bullet)
    {
        bulletObjectPool.ReturnObjectToPool(bullet);
    }

    private void Shoot(Transform playersTransform)
    {
        Transform targetTransform = FindNearestShapeOfSameType()?.transform;

        if (targetTransform != null)
        {
            var bullet = bulletObjectPool.GetObjectFromPool();
            var diffPos = (targetTransform.position - playersTransform.position);

            bullet.transform.position = playersTransform.position;

            var bulletRB = bullet.GetComponent<Rigidbody2D>();
            bulletRB.AddForce(diffPos * bulletSpeed, ForceMode2D.Force);            
        }
    }

    private GameObject FindNearestShapeOfSameType()
    {
        return spawnController.GetEnemySpawnObjectOfShapeType(Player1.GetSpriteData().shapesType);
    }

    public void Update(Transform playersTransform)
    {
        currentTime += Time.deltaTime;
        if (currentTime > nextFireRate)
        {
            currentTime = 0f;
            Shoot(playersTransform);
        }
    }
}