using UnityEngine;

public class Player1Controller
{
    private ShapeController Player1;
    private SpawnObjectsController spawnController;
    private BulletProjectileController bulletProjectileController;

    public BulletProjectileController BulletProjectileController { get => bulletProjectileController; }

    public Player1Controller(ShapeController Player1, GameObject bulletPrefab, SpawnObjectsController spawnObjectsController)
    {
        this.Player1 = Player1;
        spawnController = spawnObjectsController;
        bulletProjectileController = new BulletProjectileController(Player1,"BulletPool", bulletPrefab, spawnController);
    }

    public void Update()
    {
        bulletProjectileController.Update(Player1.transform);
    }
}
