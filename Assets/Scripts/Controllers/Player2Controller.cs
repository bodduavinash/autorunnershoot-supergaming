using System;
using UnityEngine;

public class Player2Controller
{
    private ShapeController Player2;
    private SpawnObjectsController spawnController;

    public Player2Controller(ShapeController Player, SpawnObjectsController spawnObjectsController)
    {
        Player2 = Player;
        spawnController = spawnObjectsController;
    }

    public void OnTriggerEnter2D(Collider2D collision, SpriteShapesSO shapesSO)
    {
        var colliderSprite = collision.gameObject.GetComponent<SpriteRenderer>().sprite.name;

        var colliderShapeData = shapesSO.spriteList.Find(x => x.sprite.name.Equals(colliderSprite));

        OnPlayerTriggerEnter2D(collision, colliderShapeData.shapesType, Player2.GetSpriteData().shapesType);
    }

    private void OnPlayerTriggerEnter2D(Collider2D collision, ShapeTypeEnum colliderShapeType, ShapeTypeEnum playerShapeType)
    {
        if (colliderShapeType != playerShapeType)
        {
            UIManager.Instance.ShowGameoverUI(true);
        }
        else if (colliderShapeType == playerShapeType)
        {
            Enum.TryParse<LaneEnum>(collision.transform.parent.name.ToString(), out var lane);
            spawnController.ReturnObstacleObjectToPool(collision.gameObject);

            spawnController.SpawnObjects(lane, ObjectTypeEnum.Obstacle);
        }
    }
}