using UnityEngine;

[System.Serializable]
public class SpriteShapeData
{
    public ShapeTypeEnum shapesType;
    public Sprite sprite;
}