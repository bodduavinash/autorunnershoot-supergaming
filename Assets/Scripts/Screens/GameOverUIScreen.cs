using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUIScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject gameOverPanel;

    private void Start()
    {
        ShowGameOverScreen(false);
    }

    private void PauseGame(bool isPaused)
    {
        Time.timeScale = isPaused ? 0f : 1f;
    }

    public void RestartGame()
    {
        //load the scene again
        SceneManager.LoadScene(0);
        PauseGame(false);
    }

    public void ShowGameOverScreen(bool show)
    {
        gameOverPanel?.SetActive(show);
        PauseGame(show);
    }
}
