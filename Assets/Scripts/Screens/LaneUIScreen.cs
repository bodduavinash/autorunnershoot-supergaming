using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneUIScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject laneUIPanel;

    public void SelectedLane(int laneNo)
    {
        PlayerController.Instance.SpawnController.InitSpawnObjects((LaneEnum)laneNo);
        PlayerController.Instance.SetPlayerLane((LaneEnum)laneNo);
        ShowLaneUIScreen(false);
    }

    public void ShowLaneUIScreen(bool show)
    {
        laneUIPanel?.SetActive(show);
    }
}
