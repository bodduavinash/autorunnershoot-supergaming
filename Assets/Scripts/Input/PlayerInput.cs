using UnityEngine;

public class PlayerInput : IUpdatePlayerInput
{
    private readonly uint noOfObstacleShapes = 3;
    private readonly uint noOfEnemyShapes = 3;

    private uint currentObstacleShape = 0;
    private uint currentEnemyShape = 0;

    private IChangeShape Player1;
    private IChangeShape Player2;    

    public PlayerInput(IChangeShape Player1, IChangeShape Player2)
    {
        this.Player1 = Player1;
        this.Player2 = Player2;
    }

    public void OnUpdatePlayerInput()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            Player1.ChangeShape((ShapeTypeEnum)(--currentEnemyShape % noOfEnemyShapes));
        }
        else if(Input.GetKeyDown(KeyCode.E))
        {
            Player1.ChangeShape((ShapeTypeEnum)(++currentEnemyShape % noOfEnemyShapes));
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            Player2.ChangeShape((ShapeTypeEnum)(--currentObstacleShape % noOfObstacleShapes));
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            Player2.ChangeShape((ShapeTypeEnum)(++currentObstacleShape % noOfObstacleShapes));
        }
    }
}