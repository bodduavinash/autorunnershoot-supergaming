using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool
{
    #region Properties
    // Stores the list of gameobjects pool
    private List<GameObject> objectPool;
    private GameObject parentGameObject;

    public string poolName;
    #endregion

    #region Public Methods

    public GameObjectPool(string poolName)
    {
        this.poolName = poolName;
    }

    /// <summary>
    /// Initialize the given number of GameObjects and assign into pool.
    /// </summary>
    /// <param name="prefabGameObject"></param> Instantiates the gameobject into pool
    /// <param name="numberOfGameObjects"></param> assign the number of gameobjects in pool
    public void InitGameObjectsPool(GameObject prefabGameObject, int numberOfGameObjects)
    {
        if (objectPool != null)
        {
            ClearGameObjectsPool();
        }

        objectPool = new List<GameObject>(numberOfGameObjects);
        parentGameObject = new GameObject(poolName + "GameObjectPool");

        if (objectPool != null)
        {
            for (var i = 0; i < numberOfGameObjects; i++)
            {
                objectPool.Add(GameObject.Instantiate(prefabGameObject, Vector3.zero, Quaternion.identity));
                objectPool[i].name += "__" + (i + 1);
                objectPool[i].SetActive(false);
                objectPool[i].transform.parent = parentGameObject.transform;
            }
        }
    }

    /// <summary>
    /// Clear gameobjects pool
    /// </summary>
    public void ClearGameObjectsPool()
    {
        objectPool?.Clear();
        objectPool = null;
    }

    /// <summary>
    /// Get the gameobject from last one in the pool and return the gameobject.
    /// </summary>
    /// <returns></returns>
    public GameObject GetObjectFromPool()
    {
        if (objectPool.Count < 1)
        {
            Debug.LogError($"Object Pool is Empty, try putting back items to the pool.");
            return null;
        }

        var gameObject = objectPool[objectPool.Count - 1];
        objectPool.Remove(gameObject);
        gameObject.SetActive(true);
        return gameObject;
    }

    /// <summary>
    /// Add the object into pool.
    /// </summary>
    /// <param name="object"></param>
    public void ReturnObjectToPool(GameObject @object)
    {
        @object.SetActive(false);
        objectPool?.Add(@object);
        @object.transform.parent = parentGameObject.transform;
    }
    #endregion
}
