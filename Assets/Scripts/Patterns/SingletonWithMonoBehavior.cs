using UnityEngine;

public class SingletonWithMonoBehavior<T> : MonoBehaviour where T :SingletonWithMonoBehavior<T>
{
    #region Properties
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
            }
            if (instance == null)
            {
                Debug.LogError($"Singleton <{typeof(T)} {instance} > instance has not been found");
            }

            return instance;
        }
    }
    #endregion

    #region Private Methods
    private void Awake()
    {
        if(instance == null)
        {
            instance = this as T;
        }
    }
    #endregion
}
